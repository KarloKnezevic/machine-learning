package learning.supervised.classification.Knn;

import learning.supervised.bean.LearningDataPair;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 7/5/13
 * Time: 10:51 AM
 * To change this template use File | Settings | File Templates.
 */
public class EucledianDistance extends Distance {
    public double getDistance(LearningDataPair a, LearningDataPair b) {
        int numAttributes = a.getNumAttributes();
        double distance = 0;
        for (int i = 0; i < numAttributes; i++) {
            double x = a.value(i);
            double y = b.value(i);
            distance += (x - y) * (x - y);
        }
        return Math.sqrt(distance);
    }

    @Override
    public void setPrecedence(boolean MIN_PRECEDENCE) {
        this.MIN_PRECEDENCE=true;
    }
}
