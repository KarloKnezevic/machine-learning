package learning.supervised.classification.Knn;

import learning.supervised.bean.LearningDataPair;
import learning.supervised.bean.LearningDataSet;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

/**
 * User: prashanth.v
 * Date: 7/5/13
 * Time: 10:45 AM
 */
public abstract class KnnClassifier {
    int k;
    boolean isNormalized;
    LearningDataSet trainingSet;
    int weightType;
    public static final int WEIGHT_NONE = 1;
    Distance distanceFunction;
    public static final int WEIGHT_INVERSE = 2;
    TreeMap<Double, ArrayList<LearningDataPair>> distanceMap;


    public KnnClassifier(int k, LearningDataSet trainingSet, Distance distanceFunction,boolean isNormalized) {
        if (k <= 0)
            throw new IllegalArgumentException("K must be greater than zero!");
        this.k = k;
        this.trainingSet = trainingSet;
        this.weightType = WEIGHT_NONE;
        this.distanceFunction = distanceFunction;
        this.isNormalized=isNormalized;
        final boolean isMinPrecedent = distanceFunction.isMIN_PRECEDENCE();
        this.distanceMap = new TreeMap<Double, ArrayList<LearningDataPair>>(new Comparator<Double>() {
            public int compare(Double o1, Double o2) {
                if (!isMinPrecedent) {
                    return -(o1).compareTo(o2);
                }
                return (o1).compareTo(o2);
            }
        });
    }

    public abstract double classify(LearningDataPair testLearningDataPair);

    public ArrayList<LearningDataPair> getNearestNeighbours(LearningDataPair newLearningDataPair) {
        if(!isNormalized){

        }
        ArrayList<LearningDataPair> neighbors = new ArrayList<LearningDataPair>();
        for (LearningDataPair dataPair : trainingSet.getDataSet()) {
            double distance = distanceFunction.getDistance(dataPair, newLearningDataPair);
            addDataToListMap(distanceMap, dataPair, distance);
        }
        System.out.println("LearningDataPair:" + newLearningDataPair);
        System.out.println("Neighbors:");

        for (Map.Entry<Double, ArrayList<LearningDataPair>> neighbor : distanceMap.entrySet()) {
            if (neighbors.size() + neighbor.getValue().size() < k)  {
                neighbors.addAll(neighbor.getValue());

            }
            else {
                int diff = k - neighbors.size() - 1;
                for (int i = 0; i < neighbor.getValue().size(); i++) {
                    neighbors.add(neighbor.getValue().get(i));
                    if (i == diff) break;
                }
            }
            if (neighbors.size() >= k) break;

        }

        return neighbors;
    }


    public LearningDataSet getTrainingSet() {
        return trainingSet;
    }

    public void setTrainingSet(LearningDataSet trainingSet) {
        this.trainingSet = trainingSet;
    }

    public void setWeightType(int weightType) {
        this.weightType = weightType;
    }

    public void addDataToListMap(TreeMap<Double, ArrayList<LearningDataPair>> map, LearningDataPair a, double key) {
        ArrayList<LearningDataPair> pairList = map.get(key);
        if (pairList == null) {
            pairList = new ArrayList<LearningDataPair>();
        }
        pairList.add(a);
        map.put(key, pairList);
    }

    public double getWeight(double distance) {
        if (weightType == WEIGHT_NONE)
            return 1;
        else {
            if (distance == 0)
                return Double.NaN;
            else
                return 1 / distance;
        }
    }

    public static void main(String[] args) throws Exception {
        double[][] inputs=new double[][]{{3,1},{8,9},{9,9},{8.5,9},{2,1.5},{3.5,2},{2,1},{2.5,2},{3,5},{4,4.5},{9,2},{6,3},{2.5,5},{4.3,2},{2.4,4},{4.5,6}};
        double [][]outputs=new double[][]{{0},{3},{3},{3},{0},{0},{0},{0},{1},{1},{2},{1},{1},{0},{1},{1}};
        double[] input=new double[]{10.55,11.75};
        double[]output=new double[1];
        LearningDataSet set=new LearningDataSet(inputs,outputs);
        set.normalizeData();
        KnnClassifier classifier=new DiscreteKnnClassifier(3,set,new EucledianDistance(),4,true);

        System.out.println(classifier.classify(new LearningDataPair(input, output)));
    }
}
