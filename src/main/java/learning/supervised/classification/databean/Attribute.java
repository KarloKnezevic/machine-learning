package learning.supervised.classification.databean;

import java.util.HashMap;

/**
 * User: prashanth.v
 * Date: 7/6/13
 * Time: 9:06 PM
 */
public class Attribute {
    Double[] data;
    int index;
    HashMap<Double, Integer> uniqueAttributes;
    int numAttributes;
    int numValues;
    AttributeType type;
    boolean isUsed;

    public Attribute(Double[] data, int idx) {
        this.data = data;
        this.index = idx;
        uniqueAttributes = new HashMap<Double, Integer>();
        for (Double value : data) {
            int count = uniqueAttributes.get(value) == null ? 0 : uniqueAttributes.get(value) + 1;
            uniqueAttributes.put(value, count);
        }
        type=AttributeType.CONTINUOUS;
        numValues = uniqueAttributes.size();
        this.isUsed=false;
    }

    public AttributeType getType() {
        return type;
    }

    public boolean isUsed(){
           return isUsed;
    }
    public void setType(AttributeType type) {
        this.type = type;
    }

    public Attribute(Double[] data, HashMap<Double, Integer> uniqueAttributes, int idx) {
        this.data = data;
        this.uniqueAttributes = uniqueAttributes;
        this.numValues = uniqueAttributes.size();
        this.index = idx;
        this.isUsed=false;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Double[] getData() {
        return data;
    }

    public void setData(Double[] data) {
        this.data = data;
    }

    public HashMap<Double, Integer> getUniqueAttributes() {
        return uniqueAttributes;
    }

    public void setUniqueAttributes(HashMap<Double, Integer> uniqueAttributes) {
        this.uniqueAttributes = uniqueAttributes;
    }

    public int getNumAttributes() {
        return numAttributes;
    }

    public void setNumAttributes(int numAttributes) {
        this.numAttributes = numAttributes;
    }

    public int getNumValues() {
        return numValues;
    }

    public void setNumValues(int numValues) {
        this.numValues = numValues;
    }

    public Integer getCount(int index) {
        return uniqueAttributes.get(data[index]);
    }
    public Double getValue(int idx){
        return data[idx];
    }

    public void setUsed(boolean used) {
        isUsed = used;
    }
}
