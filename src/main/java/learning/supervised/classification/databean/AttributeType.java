package learning.supervised.classification.databean;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 7/8/13
 * Time: 7:44 PM
 * To change this template use File | Settings | File Templates.
 */
public enum AttributeType {
    DISCRETE,
    CONTINUOUS
}
