package learning.supervised.artificialNeuralNetworks.algorithms;

import learning.supervised.artificialNeuralNetworks.ANN;
import learning.supervised.artificialNeuralNetworks.Layer;
import learning.supervised.artificialNeuralNetworks.Neuron;
import learning.supervised.artificialNeuralNetworks.Synapse;
import learning.supervised.bean.LearningDataSet;

import java.util.Arrays;

/**
 * User: prashanth.v
 * Date: 7/3/13
 * Time: 4:42 PM
 */
public class ResilientPropogation extends Propogation {
    enum RpropType {
        RPROPp
    }

    public double updateValues[];

    public static final double POSITIVE_ETA = 1.5;
    public static final double DEFAULT_ZERO_TOLERANCE = 0.00000000000000001;
    public static final double NEGATIVE_ETA = 0.6;
    public static final double DELTA_MIN = 1e-6;
    public static final double DEFAULT_INITIAL_UPDATE = 0.2;
    public static final double maxStep = 50;
    RpropType rpropType = RpropType.RPROPp;

    public ResilientPropogation(ANN network, LearningDataSet dataset) {
        super(network, dataset);
        updateValues = new double[network.getNeuronCount() + 1];
        Arrays.fill(updateValues, DEFAULT_INITIAL_UPDATE);
    }

    @Override
    public void iteration(int count) {
        super.iteration(count);    //To change body of overridden methods use File | Settings | File Templates.
    }


    public double updateWeight(double[] gradients, double[] lastGradient, int i) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void updateWeights(Layer layer) {
        super.updateWeights(layer);    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    public void updateWeight(Neuron n, Synapse syn) {
        //To change body of implemented methods use File | Settings | File Templates.
        double weightChange = 0;

        switch (this.rpropType) {
            case RPROPp:
                weightChange = updateWeightPlus(n, syn);
                break;

        }
        double netWeight = syn.getWeight() + weightChange;
        System.out.println("changeee:" + netWeight + " " + syn.getWeight() + " " + weightChange);
        syn.setWeight(netWeight);


    }

    int sign(double x) {
        if (x < 0) {
            return -1;
        } else if (x > 0) {
            return 1;
        }
        return 0;
    }

    public double updateWeightPlus(Neuron n,
                                   Synapse syn) {
        double gradient = syn.getGradient();
        System.out.println(syn.getSrc().id + "  ==> " + syn.getDestination().id + " " + syn.getGradient() + " " + syn.getPrevGradient());
        System.out.println(gradient + " " + syn.getPrevGradient());
        double lastGradient = syn.getPrevGradient();
        final int change = sign(gradient * lastGradient);
        double weightChange = 0;
        if (change > 0) {
            double delta = syn.getUpdateValues() * POSITIVE_ETA;
            delta = Math.min(delta, maxStep);
            weightChange = -sign(gradient) * delta;
            syn.setUpdateValues(delta);
            lastGradient = gradient;
        } else if (change < 0) {
            double delta = syn.getUpdateValues() * NEGATIVE_ETA;
            System.out.println("b4:" + delta + " " + syn.getUpdateValues());
            delta = Math.max(delta, DELTA_MIN);
            System.out.println(delta + " after");
            syn.setUpdateValues(delta);
            //  syn.setGradient(0);
            lastGradient = 0;
        } else if (change == 0) {
            final double delta = syn.getUpdateValues();
            weightChange = -sign(gradient) * delta;
            lastGradient = gradient;

        }
        syn.setGradient(lastGradient);
        //syn.setPrevGradient(lastGradient);
        syn.setDeltaWt(weightChange);
        return weightChange;
    }


}
