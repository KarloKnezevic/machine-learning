package learning.supervised.artificialNeuralNetworks.algorithms;

import learning.supervised.artificialNeuralNetworks.ANN;
import learning.supervised.bean.LearningDataSet;
import learning.supervised.artificialNeuralNetworks.Neuron;
import learning.supervised.artificialNeuralNetworks.Synapse;

/**
 * User: prashanth.v
 * Date: 7/3/13
 * Time: 4:42 PM
 */
public class BackPropogation extends Propogation {
    double momentum;
    double learningRate;
    double[] lastGradient;
    ANN network;

    public BackPropogation(ANN network, LearningDataSet dataSet,double learningRate, double momentum) {
        super(network,dataSet);
        this.momentum = momentum;
        this.learningRate = learningRate;
    }

    public double getMomentum() {
        return momentum;
    }


    public void setMomentum(double momentum) {
        this.momentum = momentum;
    }

    public double getLearningRate() {
        return learningRate;
    }

    public void setLearningRate(double learningRate) {
        this.learningRate = learningRate;
    }

    public void updateWeights() {

    }

    @Override
    public void iteration(int count) {
        super.iteration(count);    //To change body of overridden methods use File | Settings | File Templates.
    }


    public double updateWeight(double[] gradients, double[] lastGradient, int i) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }
    public void updateWeight(Neuron n,Synapse syn){
        double ai=syn.getSrc().getOutput();
        double deltaWt=-learningRate*syn.getGradient()*ai;
        syn.setDeltaWt(deltaWt);
        double deltaWeight=deltaWt+momentum*syn.getPrevDeltaWt();
        double netWeight=syn.getWeight()+deltaWeight;
        System.out.println(ai+" "+ai*syn.getGradient()+" "+netWeight+" "+syn.getSrc().id+" "+syn.getDestination().id+" "+syn.getWeight()+" "+syn.getGradient()+" "+syn.getPrevDeltaWt());
        syn.setWeight(netWeight);

    }
}
