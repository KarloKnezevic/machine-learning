package learning.supervised.artificialNeuralNetworks.algorithms;

import learning.supervised.artificialNeuralNetworks.ANN;
import learning.supervised.artificialNeuralNetworks.Layer;
import learning.supervised.artificialNeuralNetworks.Neuron;
import learning.supervised.artificialNeuralNetworks.Synapse;
import learning.supervised.bean.LearningDataPair;
import learning.supervised.bean.LearningDataSet;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * User: prashanth.v
 * Date: 7/3/13
 * Time: 4:42 PM
 */
abstract public class Propogation {
    ANN network;
    LearningDataSet dataset;
    protected double[] gradients;
    double[] lastGradient;
    protected double lastError=0;
    private double totalError;
    private int iteration=0;
    private double[] flatSpot;
    //private ErrorFunction ef = new LinearErrorFunction();


    protected Propogation(ANN network, LearningDataSet dataset) {
        this.network = network;
        this.dataset = dataset;
        lastError=0;
        iteration=0;
    }

    public void iteration(final int count) {

        try {
            for (int i = 0; i < count; i++) {
                for (LearningDataPair pair : dataset.getDataSet()) {
                    preIteration();
                    rollIteration();
                    Double[] desiredOutputs=pair.getDataPair().getSecond();
                    System.out.println("Desired Output:"+ Arrays.toString(desiredOutputs));
                    network.setDataPair(pair);
                    network.forwardIteration();
                    System.out.println("Obtained Outputs:" + Arrays.toString(network.getOutputs()));
                    learn((desiredOutputs));
                    this.lastError += this.getError(desiredOutputs);
                    postIteration();
                }
            }
        } catch (final Exception ex) {
            ex.printStackTrace();
        }
        iteration++;
    }

    private void postIteration() {
    }

    private double getError(Double[] desiredOutputs) {
        double[] outputs=network.getOutputs();
        double error=0;
        for(int i=0;i<desiredOutputs.length;i++) {
            error+=Math.pow(desiredOutputs[i]-outputs[i],2);
        }
        return error;
    }

    private void learn(Double[] desiredOutputs) {
        calculateGradients(desiredOutputs);
    }

    private void calculateGradients(Double[] desiredOutputs) {
        network.getOutputLayer().calculateGradients(desiredOutputs);
        updateWeights(network.getOutputLayer());
        for (Layer layer : network.getHiddenLayers()) {
            layer.calculateGradients(null);
            updateWeights(layer);
        }

    }


    public void updateWeights(Layer layer) {
        int i = 0;
        for (Neuron n : layer.getNeurons()) {
            ArrayList<Synapse> synapses = n.getIncomingSynapses();
            for (Synapse syn : synapses) {
                // double ai=syn.getSrc().getOutput();
                updateWeight(n, syn);
            }
        }

    }

    abstract public void updateWeight(Neuron n, Synapse syn);


    private void rollIteration() {


    }


    private void preIteration() {


    }
}
