package learning.supervised.artificialNeuralNetworks;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 7/4/13
 * Time: 4:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class SigmoidFunction implements  ActivationFunction {
    public double activate(double sum) {
        return 1/(1+Math.exp(-sum));
    }

    public double partialDerivative(double a, double b) {
        return b*(1-b);
    }
}
