package learning.supervised.artificialNeuralNetworks;

import learning.supervised.artificialNeuralNetworks.algorithms.ResilientPropogation;
import learning.supervised.bean.LearningDataPair;
import learning.supervised.bean.LearningDataSet;
import learning.unsupervised.clustering.bean.Pair;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * User: prashanth.v
 * Date: 7/3/13
 * Time: 2:41 PM
 */
public class ANN {
    Layer inputLayer;
    Layer outputLayer;
    ArrayList<Layer> hiddenLayers;
    int noOfHiddenLayers = 0;
    boolean biasAccepted = true;
    Double[] desiredOutput;
    int neuronCount=0;


    public ANN(int noOfInputNeurons, int noOfOutputNeurons, ActivationFunction inputActivation, ActivationFunction outputActivation) {
        this.inputLayer = new Layer(noOfInputNeurons, LayerType.INPUT_LAYER, inputActivation, null, null, false);
        this.outputLayer = new Layer(noOfOutputNeurons, LayerType.OUTPUT_LAYER, outputActivation, null, null, biasAccepted);
        hiddenLayers = new ArrayList<Layer>();
        neuronCount+=(noOfInputNeurons+noOfOutputNeurons);
    }

    public ANN(int noOfInputNeurons, int noOfOutputNeurons, int noOfHiddenLayers, ActivationFunction inputActivation,
               ActivationFunction outputActivation, ActivationFunction commonHiddenActivation, int... noOfHiddenNeurons) throws Exception {
        if (noOfHiddenNeurons.length < noOfHiddenLayers) {
            throw new Exception("Mismatch between no of hidden layers and no of hidden layer count");
        }
        this.inputLayer = new Layer(noOfInputNeurons, LayerType.INPUT_LAYER, inputActivation, null, null, biasAccepted);
        hiddenLayers = new ArrayList<Layer>(noOfHiddenLayers);
        Layer prevLayer = inputLayer;
        for (int i = 0; i < noOfHiddenLayers; i++) {
            neuronCount+=noOfHiddenNeurons[i];
            Layer currentLayer = new Layer(noOfHiddenNeurons[i], LayerType.HIDDEN_LAYER, commonHiddenActivation, prevLayer, null, biasAccepted);
            prevLayer.setNext(currentLayer);
            hiddenLayers.add(currentLayer);
            prevLayer = currentLayer;
        }

        this.inputLayer.setNext(hiddenLayers.get(0));
        Layer lastHiddenLayer = hiddenLayers.get(hiddenLayers.size() - 1);
        this.outputLayer = new Layer(noOfOutputNeurons, LayerType.OUTPUT_LAYER, outputActivation, lastHiddenLayer, null, biasAccepted);
        lastHiddenLayer.setNext(outputLayer);
    }

    public void addHiddenLayer(int neuronCount, ActivationFunction activationFunction) {
        Layer prevLayer = inputLayer;
        if (hiddenLayers.size() > 0) {
            prevLayer = hiddenLayers.get(hiddenLayers.size() - 1);
        }
        this.neuronCount+=neuronCount;
        Layer currentLayer = new Layer(neuronCount, LayerType.HIDDEN_LAYER, activationFunction, prevLayer, null, biasAccepted);
        prevLayer.setNext(currentLayer);
        currentLayer.setNext(outputLayer);
        outputLayer.setPrevious(currentLayer);
        hiddenLayers.add(currentLayer);
        noOfHiddenLayers++;
    }

    public void setTrainingSet(LearningDataSet dataSet) {
        for (LearningDataPair dataPair : dataSet.getDataSet()) {


        }
    }

    public void setDataPair(LearningDataPair dataPair) {
        Pair<Double[], Double[]> dataPairs = dataPair.getDataPair();
        Double[] inputs = dataPairs.getFirst();
        Double[] outputs = dataPairs.getSecond();
        int i = 0;
        for (Neuron neuron : inputLayer.getNeurons()) {
            neuron.setOutput(inputs[i++]);
        }
        i = 0;
        for (Neuron neuron : outputLayer.getNeurons()) {
            neuron.setOutput(outputs[i++]);
        }
        desiredOutput = outputs;
    }

    public void forwardIteration() {
        activateHiddenNeurons();
        activateOutPutNeurons();
    }

    private void activateOutPutNeurons() {
        outputLayer.activate();
    }

    private void activateHiddenNeurons() {
        for (Layer layer : hiddenLayers) {
            layer.activate();
            System.out.println(Arrays.toString(layer.getOutputs()));

        }
    }

    public double[] getOutputs() {
        double[] output = new double[outputLayer.getNeuronCount()];
        for (int i = 0; i < output.length; i++) {
            output[i] = outputLayer.getNeurons().get(i).getOutput();
        }
        return output;
    }

    public Layer getInputLayer() {
        return inputLayer;
    }

    public void setInputLayer(Layer inputLayer) {
        this.inputLayer = inputLayer;
    }

    public Layer getOutputLayer() {
        return outputLayer;
    }

    public void setOutputLayer(Layer outputLayer) {
        this.outputLayer = outputLayer;
    }

    public ArrayList<Layer> getHiddenLayers() {
        return hiddenLayers;
    }

    public void setHiddenLayers(ArrayList<Layer> hiddenLayers) {
        this.hiddenLayers = hiddenLayers;
    }

    public int getNoOfHiddenLayers() {
        return noOfHiddenLayers;
    }

    public Double[] getDesiredOutput() {
        return desiredOutput;
    }

    public void setDesiredOutput(Double[] desiredOutput) {
        this.desiredOutput = desiredOutput;
    }

    public int getNeuronCount() {
        return neuronCount;
    }

    public void setNeuronCount(int neuronCount) {
        this.neuronCount = neuronCount;
    }

    public void setNoOfHiddenLayers(int noOfHiddenLayers) {
        this.noOfHiddenLayers = noOfHiddenLayers;
    }

    public boolean isBiasAccepted() {
        return biasAccepted;
    }

    public void setBiasAccepted(boolean biasAccepted) {
        this.biasAccepted = biasAccepted;
    }

    public static void main(String[] args) throws Exception {
        ANN network = new ANN(2, 1, 1, new SigmoidFunction(), new SigmoidFunction(), new SigmoidFunction(), 2);
        final double inputs[][] = {{1, 1}, {1, 0}, {0, 1}, {0, 0}};
        final double expectedOutputs[][] = {{0}, {1 }, {1 }, {0}};
        LearningDataSet dataSet = new LearningDataSet(inputs, expectedOutputs);
        ResilientPropogation propogation = new ResilientPropogation(network, dataSet);//, 0.9, 0.7);
        propogation.iteration(1000);
    }
}
