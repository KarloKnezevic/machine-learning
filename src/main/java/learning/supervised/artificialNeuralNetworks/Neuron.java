package learning.supervised.artificialNeuralNetworks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 7/3/13
 * Time: 1:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class Neuron {
    static int neuronCount = 0;
    final public int id;
    ArrayList<Synapse> incomingSynapses;
    HashMap<Integer, Synapse> connectionLookup;
    Synapse biasSynapse;
    double bias = -1;
    double output;

    public Neuron() {
        id = neuronCount;
        neuronCount++;
        incomingSynapses = new ArrayList<Synapse>();
        connectionLookup = new HashMap<Integer, Synapse>();
    }

    public double calculateOutput(ActivationFunction function) {
        double sum = 0;
        for (Synapse synapse : incomingSynapses) {
            double weight = synapse.getWeight();

            Neuron neuron = synapse.getSrc();
            sum += weight * neuron.getOutput();
            System.out.print("\n"+synapse.src.id+" "+synapse.destination.id+" "+weight+" "+neuron.getOutput());
        }
        sum += biasSynapse.getWeight() * bias;
        System.out.print(" "+sum +" "+function.activate(sum)+"\n");
        return function.activate(sum);
    }

    public void addIncomingSynapses(ArrayList<Neuron> prevLayerNeurons, Random random) {
        for (Neuron n : prevLayerNeurons) {
            double rand=getRandom(random);
            System.out.println(rand);
            Synapse con = new Synapse(n, this,rand);
            incomingSynapses.add(con);
            connectionLookup.put(n.id, con);
        }
    }

    public double getRandom(Random random) {
        return 1 * (random.nextDouble() * 2 - 1);
    }

    public Synapse getConnection(int neuronIndex) {
        return connectionLookup.get(neuronIndex);
    }

    public void addIncomingSynapse(Synapse synapse) {
        incomingSynapses.add(synapse);
    }

    public void addBiasConnection(Synapse synapse) {
        biasSynapse = synapse;
    }


    public ArrayList<Synapse> getIncomingSynapses() {
        return incomingSynapses;
    }

    public void setIncomingSynapses(ArrayList<Synapse> incomingSynapses) {
        this.incomingSynapses = incomingSynapses;
    }

    public Synapse getBiasSynapse() {
        return biasSynapse;
    }

    public void setBiasSynapse(Synapse biasSynapse) {
        this.biasSynapse = biasSynapse;
    }

    public double getBias() {
        return bias;
    }

    public void setBias(double bias) {
        this.bias = bias;
    }

    public double getOutput() {
        return output;
    }

    public void setOutput(double output) {
        this.output = output;
    }
}
