package learning.supervised.artificialNeuralNetworks;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 7/3/13
 * Time: 2:48 PM
 * To change this template use File | Settings | File Templates.
 */
public enum  LayerType {
    INPUT_LAYER,
    HIDDEN_LAYER,
    OUTPUT_LAYER
}
