package learning.supervised.artificialNeuralNetworks;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 7/3/13
 * Time: 1:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class Synapse {
    Neuron src;
    Neuron destination;
    double weight;
    double gradient;
    double prevGradient;
    double deltaWt;
    double prevDeltaWt;
    double updateValues;
        //double prevDeltaWt;
    static int synapseCount = 0;
    final public int id;

    public Synapse(Neuron src, Neuron destination) {
        this.src = src;
        this.destination = destination;
        this.weight = 0;
        this.gradient = 0;
        this.deltaWt=0;
        this.prevDeltaWt=0;
        this.prevGradient = 0;
        this.updateValues=0.09;
        id=synapseCount;
        synapseCount++;
    }
    public Synapse(Neuron src, Neuron destination,double weight) {
        this.src = src;
        this.destination = destination;
        this.weight = weight;
        this.gradient = 0;
        this.prevGradient = 0;
        id=synapseCount;
        this.prevDeltaWt=0;
        this.deltaWt=0;
        this.updateValues=0.09;
        synapseCount++;
    }

    public Neuron getSrc() {
        return src;
    }

    public void setSrc(Neuron src) {
        this.src = src;
    }

    public Neuron getDestination() {
        return destination;
    }

    public void setDestination(Neuron destination) {
        this.destination = destination;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getGradient() {
        return gradient;
    }

    public double getDeltaWt() {
        return deltaWt;
    }

    public void setDeltaWt(double deltaWt) {
        System.out.println(this.deltaWt+" here: "+deltaWt);
        this.prevDeltaWt=this.deltaWt;
        this.deltaWt = deltaWt;
    }

    public double getPrevDeltaWt() {
        return prevDeltaWt;
    }

    public void setPrevDeltaWt(double prevDeltaWt) {
        this.prevDeltaWt = prevDeltaWt;
    }

    public static int getSynapseCount() {
        return synapseCount;
    }

    public static void setSynapseCount(int synapseCount) {
        Synapse.synapseCount = synapseCount;
    }

    public void setGradient(double gradient) {
        this.prevGradient = this.gradient;
        this.gradient = gradient;
    }

    public double getPrevGradient() {
        return prevGradient;
    }

    public void setPrevGradient(double prevGradient) {
        this.prevGradient = prevGradient;
    }

    public double getUpdateValues() {
        return updateValues;
    }

    public void setUpdateValues(double updateValues) {
        this.updateValues = updateValues;
    }
}
