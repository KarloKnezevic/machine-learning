package learning.supervised.bean;

import learning.unsupervised.clustering.bean.Pair;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 7/3/13
 * Time: 8:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class LearningDataPair
{
    Pair<Double[], Double[]> dataPair;
    int numAttributes;



    public LearningDataPair(Pair<Double[], Double[]> dataPair) {
        this.dataPair = dataPair;
        assignNumAttributes(dataPair);
    }

    private void assignNumAttributes(Pair<Double[], Double[]> dataPair) {
        numAttributes = checkAvailability(0) ? dataPair.getFirst().length : 0;
    }


    public LearningDataPair(Double[] input, Double[] output) {
        this.dataPair = new Pair<Double[], Double[]>(input, output);
        assignNumAttributes(dataPair);
    }

    public LearningDataPair(double[] input, double[] outputs) {
        Double[] features = new Double[input.length];
        Double[] outputList = new Double[outputs.length];
        int i = 0;
        for (double inputFeature : input) {
            features[i++] = inputFeature;
        }
        i = 0;
        for (double output : outputs) {
            outputList[i++] = output;
        }
        this.dataPair = new Pair<Double[], Double[]>(features, outputList);
        assignNumAttributes(dataPair);
    }

    public Pair<Double[], Double[]> getDataPair() {
        return dataPair;
    }

    public void setDataPair(Pair<Double[], Double[]> dataPair) {
        this.dataPair = dataPair;
    }

    boolean checkAvailability(int length) {
        return dataPair != null && dataPair.getFirst() != null && dataPair.getFirst().length > length;
    }
    boolean checkAvailabilityOutput(int length) {
            return dataPair != null && dataPair.getSecond() != null && dataPair.getSecond().length > length;
        }
    public Double value(int idx) {
        return checkAvailability(idx) ? dataPair.getFirst()[idx] : null;
    }

    public Double output(int idx) {
        return checkAvailabilityOutput(idx) ? dataPair.getSecond()[idx] : null;
    }

    public int getNumAttributes() {
        return numAttributes;
    }

    public void setNumAttributes(int numAttributes) {
        this.numAttributes = numAttributes;
    }

    public Double[] getInput() {
        return dataPair.getFirst();
    }

    public Double[] getIdeal() {
        return dataPair.getSecond();
    }
    public int getOutputSize(){
         return checkAvailabilityOutput(0)?dataPair.getSecond().length:0;
    }

    public Double getIdeal(int idx) {
        return output(idx);
    }
    public void normalizeData(Double[] max,Double[] min){
        Double[] data=dataPair.getFirst();
        for(int i=0;i<data.length;i++){
           data[i]=(data[i]-min[i])/(max[i]-min[i]);
        }
    }


}
