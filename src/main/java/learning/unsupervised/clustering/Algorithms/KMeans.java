package learning.unsupervised.clustering.Algorithms;

import learning.unsupervised.clustering.bean.*;

import java.util.*;

/**
 * User: prashanth.v
 * Date: 6/19/13
 * Time: 5:04 PM
 */
public class KMeans extends Clustering {
   /* Double dataSet[][];
    int cluster;
    Map<Integer, ClusterDataPoint> clusterMap = new HashMap<Integer, ClusterDataPoint>();
    ExecutorService executorService = Executors.newFixedThreadPool(20);
*/
    public KMeans(Double dataSet[][], int k) {
        super(dataSet, k);
    }

    public void calculateCentroids(ClusterCenters clusterCenters) {
        clusterCenters.setDataValues(calculateCentroid(clusterCenters.getMemberPoints()));
    }


    public Double[] calculateCentroid(HashSet<ClusterDataPoint> dataPts) {
        int featureLength = clusterData.getDataSet().get(0).getDataValues().length;
        int dataSize = dataPts.size();
        if(dataSize==0) dataSize=1;
        Double centroids[] = new Double[featureLength];
        Arrays.fill(centroids, 0.0);
        for (int j = 0; j < featureLength; j++) {
            for (ClusterDataPoint dataPt : dataPts) {
                centroids[j] += dataPt.getDataValues()[j];
            }
            centroids[j] /= dataSize;
        }
        return centroids;
    }

    public void assignToClusters() {
        ArrayList<ClusterDataPoint> dataSet = clusterData.getDataSet();
        for (ClusterDataPoint dataPoint : dataSet)
            try {
                Pair<Integer, Double> clusterDistance = selectBestCluster(dataPoint);
                modifyCluster(dataPoint, clusterDistance);
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public void modifyCluster(ClusterDataPoint dataPoint, Pair<Integer, Double> bestCluster) {
        HashMap<Integer,ClusterCenters> clusterMap = clusterData.getClusterCenters();
        int associatedCluster = dataPoint.getAssociatedCluster();
        ClusterCenters oldCenters = associatedCluster != -1 ? clusterMap.get(associatedCluster) : null;
        ClusterCenters newCenters = clusterMap.get(bestCluster.getFirst());
        if (oldCenters != null) {
            oldCenters.getMemberPoints().remove(dataPoint);
        }
        dataPoint.setDistance(bestCluster.getSecond());
        dataPoint.setAssociatedCluster(bestCluster.getFirst());
        /*  if (newCenters == null) {
            newCenters = new ClusterCenters(dataPoint, bestCluster.getFirst());
        }*/
        newCenters.getMemberPoints().add(dataPoint);
    }

    public double calculateDistance(Double[] a, Double[] b) {
        if (a.length != b.length) {
            throw new IllegalArgumentException("The dimensions have to be equal!");
        }
        double sum = 0.0;
        for (int i = 0; i < a.length; i++) {
            sum += Math.pow(a[i] - b[i], 2);
        }
        return Math.sqrt(sum);
    }


    public Pair<Integer, Double> selectBestCluster(final DataPoint dataPoint) {
        ArrayList<Pair<Integer, Double>> clusterDistances = calculateCentroidDistances(dataPoint);
        return Collections.min(clusterDistances);
    }

    public ArrayList<Pair<Integer, Double>> calculateCentroidDistances(final DataPoint dataPoint) {
        HashMap<Integer,ClusterCenters> clusterDataMap = clusterData.getClusterCenters();
        final ArrayList<Pair<Integer, Double>> distances = new ArrayList<Pair<Integer, Double>>(clusterData.getClusterCount());
        try {
            for (final Map.Entry<Integer,ClusterCenters> center : clusterDataMap.entrySet()) {
                if (center == null|| center.getValue()==null) break;


                Double centerData[] = center.getValue().getDataValues();
                double dist = calculateDistance(dataPoint.getDataValues(), centerData);
                distances.add(new Pair<Integer, Double>(center.getValue().getId(), dist));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return distances;
    }




    public static void main(String[] args) {
        Double[][] data = new Double[][]{{10.0, 15.0}, {5.0, 10.0}, {16.0, 15.0}, {13.0, 12.5}, {12.3, 14.5}};
        /* KMeans kMeans = new KMeans();
    ClusterData clusters = new ClusterData(3);*/
        // kMeans.calculateCentroid(data);
    }

}
