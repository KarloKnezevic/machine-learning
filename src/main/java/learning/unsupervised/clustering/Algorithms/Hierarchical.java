package learning.unsupervised.clustering.Algorithms;

import learning.unsupervised.clustering.bean.*;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 7/2/13
 * Time: 1:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class Hierarchical {
    ClusterData clusterData;
    Similarity[][] C;
    Comparator<Similarity> comparator;
    boolean valid[];
    ArrayList<PriorityQueue<Similarity>> queueMap;


    public Hierarchical(Double[][] dataSet) {
        clusterData = new ClusterData(dataSet, 3);
        comparator = new HACComparator();
        queueMap = new ArrayList<PriorityQueue<Similarity>>();
        valid = new boolean[dataSet.length];
        C = new Similarity[dataSet.length][dataSet.length];
    }

    public ArrayList<Pair<Integer, Integer>> clusterData() {
        ArrayList<ClusterDataPoint> dataSet = clusterData.getDataSet();
        for (int i = 0; i < dataSet.size(); i++) {
            PriorityQueue<Similarity> similarityQueue = new PriorityQueue<Similarity>(dataSet.size(), comparator);
            for (int j = 0; j < dataSet.size(); j++) {
                C[i][j] = new Similarity(calculateSimilarity(dataSet.get(i), dataSet.get(j)), new Pair<Integer, Integer>(i, j));
                if (i != j)
                    similarityQueue.add(C[i][j]);
            }
            valid[i] = true;
            queueMap.add(similarityQueue);
            System.out.println(similarityQueue.peek().getSim()+" "+similarityQueue.peek().getIndex());

        }

        ArrayList<Pair<Integer, Integer>> A = new ArrayList<Pair<Integer, Integer>>();
        for (ClusterDataPoint aDataSet : dataSet) {
            PriorityQueue<Similarity> minQueue = Collections.min(queueMap, new Comparator<PriorityQueue<Similarity>>() {
                public int compare(PriorityQueue<Similarity> o1, PriorityQueue<Similarity> o2) {
                    Double a = o1 == null ? Double.MAX_VALUE : o1.peek() == null ? Double.MAX_VALUE : o1.peek().getSim();
                    Double b = o2 == null ? Double.MAX_VALUE : o2.peek() == null ? Double.MAX_VALUE : o2.peek().getSim();
                    return a.compareTo(b);
                }
            });

            if (minQueue != null && minQueue.peek()!=null) {
                System.out.println(minQueue.peek().getSim()+" "+minQueue.peek().getIndex());

                Integer k1 = minQueue.peek().getIndex().getFirst();
                System.out.println("First=" + k1);
                Similarity best = queueMap.get(k1).peek();
                int k2 = best.getIndex().getSecond();
                A.add(new Pair<Integer, Integer>(k1, k2));
                queueMap.set(k2, null);
                queueMap.get(k1).clear();
                for (int l = 0; l < dataSet.size(); l++) {
                    if (l != k1 && queueMap.get(l) != null) {
                        queueMap.get(l).remove(C[l][k1]);
                        queueMap.get(l).remove(C[l][k2]);
                        C[l][k1].setSim(linkSimilarity(l, k1, k2));
                        queueMap.get(l).add(C[l][k1]);
                        C[k1][l].setSim(linkSimilarity(l, k1, k2));
                        queueMap.get(k1).add(C[k1][l]);
                    }
                }
            }
        }
        return A;
    }

    private double linkSimilarity(int l, int k1, int k2) {
        ArrayList<ClusterDataPoint> dataSet = clusterData.getDataSet();
        return Math.min(C[l][k1].getSim(), C[l][k2].getSim());
    }


    private double calculateSimilarity(ClusterDataPoint clusterDataPoint, ClusterDataPoint clusterDataPoint1) {
        return MathUtil.euclideanDistance(clusterDataPoint1.getDataValues(), clusterDataPoint.getDataValues());
    }

    public static void main(String[] args) {
        Double[][] datas = new Double[][]{{2.0, 3.5}, {3.0, 5.0}, {6.0, 7.0}, {8.0, 9.0}, {1.0, 5.0}, {2.0, 5.0}, {3.0, 6.0}, {9.5, 2.0}, {0.0, 1.0}, {1.0, 2.0}, {3.0, 1.0}};
        Hierarchical h = new Hierarchical(datas);
       ClusterTree tree=new ClusterTree(1);
        tree.buildTree(h.clusterData(),datas.length);
        //System.out.println(h.clusterData().toString());

    }

}
