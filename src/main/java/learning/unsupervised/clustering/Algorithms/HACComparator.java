package learning.unsupervised.clustering.Algorithms;

import learning.unsupervised.clustering.bean.Similarity;

import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 7/2/13
 * Time: 3:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class HACComparator implements Comparator<Similarity> {
    public int compare(Similarity o1, Similarity o2) {
        return  o1.getSim().compareTo(o2.getSim());
        /*if(o1==null && o2==null) return 0;
        if(o1==null) return -1;
        if(o2==null) return 1;
        if(o1.getSim()<o2.getSim())
            return 1;
        else if(o1.getSim()>o2.getSim())
            return -1;
        return 0;  //To change body of implemented methods use File | Settings | File Templates.*/
    }
}
