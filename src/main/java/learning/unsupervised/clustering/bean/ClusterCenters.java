package learning.unsupervised.clustering.bean;

import java.util.HashSet;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 6/20/13
 * Time: 2:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class ClusterCenters extends DataPoint{
    int id;
    HashSet<ClusterDataPoint> memberPoints;
    public ClusterCenters(DataPoint dataPoint,int id){
        super(dataPoint.getDataValues());
        this.id=id;
        memberPoints=new HashSet<ClusterDataPoint>();
    }
    public ClusterCenters(Double[] dataValues, int id) {
        super(dataValues);
        this.id = id;
        memberPoints=new HashSet<ClusterDataPoint>();
    }

    public void addMemberPoint(ClusterDataPoint dataPoint){
         memberPoints.add(dataPoint);
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public HashSet<ClusterDataPoint> getMemberPoints() {
        return memberPoints;
    }

    public void setMemberPoints(HashSet<ClusterDataPoint> memberPoints) {
        this.memberPoints = memberPoints;
    }
}
