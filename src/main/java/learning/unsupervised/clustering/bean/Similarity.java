package learning.unsupervised.clustering.bean;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 7/2/13
 * Time: 2:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class Similarity {
    Double sim;
    Pair<Integer,Integer> index;

    public Similarity(double sim, Pair<Integer,Integer> index) {
        this.sim = sim;
        this.index = index;
    }

    public Double getSim() {
        return sim;
    }

    public void setSim(Double sim) {
        this.sim = sim;
    }

    public Pair<Integer,Integer> getIndex() {
        return index;
    }

    public void setIndex( Pair<Integer,Integer> index) {
        this.index = index;
    }
}
