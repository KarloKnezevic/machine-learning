package learning.unsupervised.clustering.bean;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 6/19/13
 * Time: 5:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class DataPoint {
    Double[] dataValues;

    public DataPoint(Double[] dataValues) {
        this.dataValues = dataValues;
    }

    public Double[] getDataValues() {
        return dataValues;
    }

    public void setDataValues(Double[] dataValues) {
        this.dataValues = dataValues;
    }

    @Override
    public String toString() {

       return  Arrays.toString(dataValues);
    }
}
